#!/usr/bin/env python3

import subprocess
import sys
from dataclasses import dataclass
import requests

PRETEND = False


@dataclass
class Colours:
    """This is a class to hold the ascii escape sequences for printing colours."""

    red: str = "\033[31m"
    endc: str = "\033[m"
    green: str = "\033[32m"
    yellow: str = "\033[33m"
    blue: str = "\033[34m"


def die(message: str) -> None:
    """This is a function to exit the program with a die message."""

    print(f"{Colours.red}[ERROR]{Colours.endc} {message}", file=sys.stderr)
    exit(1)


def info(message: str) -> None:
    print(f"{Colours.blue}[INFO]{Colours.endc} {message}")


def warn(message: str) -> None:
    print(f"{Colours.yellow}[WARN]{Colours.endc} {message}")


def execute(command_string: str, override: bool = False) -> str:
    if not PRETEND or override:
        command = subprocess.Popen(
            command_string,
            stdout=subprocess.PIPE,
            stderr=subprocess.DEVNULL,
            shell=True,
        )
        out, _ = command.communicate()
        return out

    print(f"[COMMAND]\n{command_string}")


def fix_config(
    config_file: dict,
    VALIDITY: dict,
    interactive: bool,
    key: str,
    valid: str,
    default: str,
):
    if not interactive:
        die(
            f"The value specified for '{key}' in the configuration file is not valid or missing. Valid options are: {valid}."
        )

    new_value = input(
        f"Enter value for '{key}'. Valid options are {valid}.\n{Colours.blue}[{default}]{Colours.endc}: "
    )
    config_file[key] = new_value if new_value != "" else default

    parse_config(config_file, VALIDITY, interactive=True)

    return config_file


def parse_config(
    config_file: dict, VALIDITY: dict, interactive: bool = False
) -> dict or str:
    """
    Parses config file and presents an interactive mode if empty config is specified.
    """

    # This code is a bit of a mess, but here is a bit of an explanation
    # VALIDITY is a dictionary with each config item. In the dictionary for each config item:
    # - func is the function to check validity
    # - mode is the validity mode
    # - default is the default value
    # - valid_text is the valid options in case of the validity mode being 'check'.
    #
    # VALIDTY MODES:
    # In the 'execute' mode, this function will execute that function and use the list it returns as a list of valid options.
    # In the 'check' mode, this function will execute the function with the value the user gives it (through config or interactive).
    # If the function returns False, the input is deemed to be invalid. If the function returns True, the input is valid.

    for key in VALIDITY:
        default = VALIDITY[key]["default"]

        match VALIDITY[key]["mode"]:
            case "execute":
                if "return" in VALIDITY[key].keys():
                    if VALIDITY[key]["return"] == True:
                        valid = VALIDITY[key]["func"](ret=config_file) 
                    else:
                        valid = VALIDITY[key]["func"]()
                else:
                    valid = VALIDITY[key]["func"]()

                if key not in config_file or config_file[key] not in valid:
                    config_file = fix_config(
                        config_file, VALIDITY, interactive, key, valid, default
                    )

            case "check":
                do_return = False
                if "return" in VALIDITY[key].keys():
                    do_return = VALIDITY[key]["return"]

                if do_return: # yes, i know this is horrific. I'm sorry.
                    if key not in config_file or not VALIDITY[key]["func"](
                        value=config_file[key], ret=config_file
                    ):
                        config_file = fix_config(
                            config_file,
                            VALIDITY,
                            interactive,
                            key,
                            VALIDITY[key]["valid_text"],
                            default,
                        )
                else:
                    if key not in config_file or not VALIDITY[key]["func"](
                        value=config_file[key]
                    ):
                        config_file = fix_config(
                            config_file,
                            VALIDITY,
                            interactive,
                            key,
                            VALIDITY[key]["valid_text"],
                            default,
                        )
                        

    return config_file


def check_url(value: str) -> bool:
    try:
        response = requests.head(value)

        if response.status_code == 200:
            return True

        warn("URL entered is not reachable. Please try again.")
    except:
        warn("URL entered is not valid - did you forget 'https://'?")

    return False


def chroot(command: str, location: str) -> None:
    execute(f'chroot {location} /bin/bash <<"EOT"\n{command}\nEOT')