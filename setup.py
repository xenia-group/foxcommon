#!/usr/bin/env python3

import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "foxcommon",
    version = "1.0.0",
    author = "Luna Deards",
    author_email = "luna@xenialinux.com",
    description= ("Common set of python utilities to be used across the foxutils."),
    license = "GPL-3",
    keywords = "common library",
    url = "https://gitlab.com/xenia-group/foxcommon",
    package_dir={"": "src"},
    packages = ["foxcommon"],
    requires=["subprocess", "sys"],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Library"
        "License :: OSI Approved :: GPL-3"
    ],
)
